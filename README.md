# README #

This is repository for postgrest Docker image

Tags created for v0.4.0.0 and v0.4.1.0

##Docker YML sample##
```
#!yaml

postgres:
  image: 'clkao/postgres-plv8:latest'
  ports:
    - '5432:5432'
postgrest:
  environment:
    - POSTGREST_ANONYMOUS=anonymous
    - POSTGREST_DBHOST=postgres
    - POSTGREST_DBNAME=postgres
    - POSTGREST_DBPASS=postgres
    - POSTGREST_DBPORT=5432
    - POSTGREST_DBUSER=postgres
    - POSTGREST_JWT_SECRET=supersecretstring
    - POSTGREST_MAX_ROWS=1000000
    - POSTGREST_POOL=20
    - POSTGREST_SCHEMA=public
  image: 'aranwe/postgrest:latest'
  links:
    - postgres
  ports:
    - '3000:3000'

```

Git with setup script and Dockerfile:
[https://bitbucket.org/skynetqms/postgrest](https://bitbucket.org/skynetqms/postgrest)

* Repo owner: bitbucket.org/4RW