
cd /etc/postgrest

echo "db-uri = \"postgres://${POSTGREST_DBUSER}:${POSTGREST_DBPASS}@${POSTGREST_DBHOST}:${POSTGREST_DBPORT}/${POSTGREST_DBNAME}\"" > ./postgrest.conf;
echo "db-schema = \"${POSTGREST_SCHEMA}\"" >> ./postgrest.conf;
echo "db-anon-role = \"${POSTGREST_ANONYMOUS}\"" >> ./postgrest.conf;
echo "jwt-secret = \"${POSTGREST_JWT_SECRET}\"" >> ./postgrest.conf;
echo "max-rows = ${POSTGREST_MAX_ROWS}" >> ./postgrest.conf;
echo "db-pool = ${POSTGREST_POOL}" >> ./postgrest.conf;
echo "pre-request = \"${POSTGREST_PRE_REQUEST}\"" >> ./postgrest.conf;
/usr/local/bin/postgrest ./postgrest.conf;
